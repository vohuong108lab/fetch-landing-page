import React from 'react'
import './Header.css'
import logo from '../../assets/logo_company.png'

const Header = () => {
    return (
        <header id="header">
            <div className="header__container">
                <a href="/">
                    <div className="header__container__logo">
                        <img className="logo__img" src={logo} alt="logo"></img>
                    </div>
                </a>
                <div className="header__container__right">
                    <ul>
                        <li>
                            <a href="/blogs">Blog</a>
                        </li>
                        <li>
                            <i aria-hidden="true" class="fab fa-whatsapp"></i>
                            <span>+65 8933 4200</span>
                        </li>
                        <li>
                            <i aria-hidden="true" class="fas fa-envelope"></i>
                            <span>sales@fetch.tech</span>
                        </li>
                    </ul>
                </div>

            </div>
        </header>
    )
}

export default Header
