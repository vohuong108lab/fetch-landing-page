import React from 'react'
import './AppRow4.css'
import img3 from '../../../assets/img3.png'
import img4 from '../../../assets/img4.png'
import newPic from '../../../assets/newPic.png'

const AppRow4 = () => {
    return (
        <div className="row4__container">
            <div className="row4__container-content">
                <div className="row4__container-row">
                    <div className="row4__container-col12">
                        <h2>3 Easy Steps to Kickstart Recruitment</h2>
                    </div>
                </div>
                <div className="row4__container-row">
                    <div className="row4__container-col4">
                        <div className="row4__container-col4-child">
                            <img src={img3} alt="" />
                            <h3>Fill In the Form or Reach Us directly through whatsapp or freshchat</h3>
                            <p>Get a response from our Tech Consultants within 1 business day</p>
                        </div>

                    </div>
                    <div className="row4__container-col4">
                        <div className="row4__container-col4-child">
                            <img src={img4} alt="" />
                            <h3>Face to face / over the phone explanation</h3>
                            <p>Expect unbiased & professional advice from us. From market trends to characteristic and risks of different engagements</p>
                            <p>Comprehensive breakdown of all cost involved, ensuring full transparency</p>
                            <p>Fairest salaries packages available in the market based on your talent needs</p>
                        </div>
                    </div>
                    <div className="row4__container-col4">
                        <div className="row4__container-col4-child">
                            <img src={newPic} alt="" />
                            <h3>Complete your Company Onboarding application</h3>
                            <p>We will start organising available talents and setup pipelines to bring in the necessary candidates for your hiring needs.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AppRow4
