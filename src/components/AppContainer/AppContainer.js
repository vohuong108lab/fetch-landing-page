import React from 'react'
import './AppContainer.css'
import AppRow1 from './AppRow1/AppRow1'
import SlickRow from './SlickRow/SlickRow'
import AppRow2 from './AppRow2/AppRow2'
import AppRow3 from './AppRow3/AppRow3'
import AppRow4 from './AppRow4/AppRow4'
import AppRow5 from './AppRow5/AppRow5'
import AppRow6 from './AppRow6/AppRow6'
import AppRow7 from './AppRow7/AppRow7'
import AppRow8 from './AppRow8/AppRow8'

const AppContainer = () => {
    return (
        <div >
            <AppRow1 />
            <SlickRow />
            <AppRow2 />
            <AppRow3 />
            <AppRow4 />
            <AppRow5 />
            <AppRow6 />
            <AppRow7 />
            <AppRow8 />
        </div>
    )
}

export default AppContainer
