import React from 'react'
import './AppRow3.css'

const AppRow3 = () => {
    return (
        <div className="row3__container">
            <div className="row3__container-content">
                <div className="row3__container-row">
                    <div className="row3__container-col12">
                        <h2>
                            More than $3,000,000
                            <br></br>
                            in salaries paid out till date
                        </h2>
                        <p>
                            Build your own tech team with our ever-growing FETCH family
                            <br></br>
                            and say goodbye to frustrating software development houses
                        </p>
                        <div className="btn-hiring">
                            <div className="btn-content">START HIRING NOW</div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    )
}

export default AppRow3
