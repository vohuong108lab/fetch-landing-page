import React from 'react'
import Slider from "react-slick";

import logoSlick1 from '../../../public/assets/suss.jpeg'
import logoSlick2 from '../../../public/assets/co.png'
import logoSlick3 from '../../../public/assets/smu.svg'
import logoSlick4 from '../../../public/assets/acc.png'
import logoSlick5 from '../../../public/assets/ke.png'
import logoSlick6 from '../../../public/assets/myeg.png'

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import './SlickRow.css'

const SlickRow = () => {
    const settings = {
        dots: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: true,
        autoplaySpeed: 1500,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                initialSlide: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]
    }

    return (
        <div className="slick__container">
            <div className="slick__container-row">
                <div className="trusted">TRUSTED BY</div>
                <div className="slick" >
                    <Slider {...settings}>
                        <div className="slick-child">
                            <img src={logoSlick1} alt="" />
                        </div>
                        <div className="slick-child">
                            <img src={logoSlick2} alt="" />
                        </div>
                        <div className="slick-child">
                            <img src={logoSlick3} alt="" />
                        </div>
                        <div className="slick-child">
                            <img src={logoSlick4} alt="" />
                        </div>
                        <div className="slick-child">
                            <img src={logoSlick5} alt="" />
                        </div>
                        <div className="slick-child">
                            <img src={logoSlick6} alt="" />
                        </div>
                    </Slider>
                </div>
            </div>
        </div>

    );
}

export default SlickRow
