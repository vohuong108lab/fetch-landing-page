import React from 'react'
import './AppRow6.css'
import qi2 from '../../../assets/qi2.jpeg'
import donal from '../../../assets/donal.jpg'
import michael from '../../../assets/michael.png'

const AppRow6 = () => {
    return (
        <div className="row6__container">
            <div className="row6__container-content">
                <div className="row6__container-row">
                    <div className="row6__container-col12">
                        <h2>Hear From Our Clients</h2>
                    </div>
                </div>
                <div className="row6__container-row">
                    <div className="row6__container-col4">
                        <div className="row6__container-col4-child">
                            <p>
                                There’s a lot of cost to hiring. Cost of going with recruitment agencies, the costs of the actual recruitment itself; actually doing the interviews. The costs of renting an office to house my employees. The operational costs of running an office. Fetch eliminates a significant chunk of the cost and it is a way to overcome our past hiring difficulties.”
                            </p>
                            <div className="row6__container-col4-bottom">
                                <img className="" src={qi2} alt="" />
                                <div className="wrapper__founder-info">
                                    <div className="founder-name">QI YU</div>
                                    <div className="founder-team">Founder Tokenize</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row6__container-col4">
                        <div className="row6__container-col4-child">
                            <p>
                                Their team was incredibly helpful in helping us find the perfect Vietnamese colleague. The work was stellar, the communication was excellent and we couldn't have been happier with the overall experience. And best of all, we didn’t have to get our hands dirty and waste precious time and resources to setup an office in Vietnam. Fetch handles all of that for us. 
                            </p>
                            <div className="row6__container-col4-bottom">
                                <img className="" src={donal} alt="" />
                                <div className="wrapper__founder-info">
                                    <div className="founder-name">DONALD</div>
                                    <div className="founder-team">Co-Founder BrdgX</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row6__container-col4">
                        <div className="row6__container-col4-child">
                            <p>
                                Fetch is amazing! We are able to find talented developers in a matter of weeks instead of months. The quality of talent they supply is extremely high as the developers are able to onboard almost immediately and ramp incredibly fast.
                            </p>
                            <div className="row6__container-col4-bottom">
                                <img className="" src={michael} alt="" />
                                <div className="wrapper__founder-info">
                                    <div className="founder-name">MICHAEL</div>
                                    <div className="founder-team">Co-Founder Stakewith.us</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AppRow6
