import React from 'react'
import './AppRow5.css'
import image7 from '../../../assets/7.jpg'
import image8 from '../../../assets/8.jpg'

const AppRow5 = () => {
    return (
        <div className="row5__container">
            <div className="row5__container-content">
                <div className="row5__container-row">
                    <div className="row5__container-colcus-left">
                        <div className="wrapper__img">
                            <img src={image7} alt="" />
                        </div>
                        <div className="wrapper__img">
                            <img src={image8} alt="" />
                        </div>

                    </div>
                    <div className="row5__container-colcus-right">
                        <div className="row5__container-colcus-child">
                            <h2>More than 200 happy companies till date</h2>
                            <p>Fetch is a big believer of employee retention and we invest heavily in making the work environment an excellent one for your employees</p>
                            <div className="row5__colcus-child-btn">
                                <div className="btn-explore">Explore More</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AppRow5
