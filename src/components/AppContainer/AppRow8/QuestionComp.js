import React, { useState } from 'react'

const QuestionComp = ({ content, arrowDown, arrowRight }) => {
    const [isClicked, setClicked] = useState(false);

    console.log('render in child')
    return (
        <div className="question-col6" onClick={() => setClicked(!isClicked)}>
            <div className="question-wrap">
                <img src={isClicked ? arrowDown : arrowRight} alt="" />
                {content ? <h3>{content.question}</h3> : <></>}
                
            </div>
            {content 
                ? 
                    <p className={`ans ${isClicked ? 'ans-active' : ''}`}>
                    {content.answer}
                    </p> 
                : <></>
            }
            
        </div>
    )
}

export default QuestionComp;
