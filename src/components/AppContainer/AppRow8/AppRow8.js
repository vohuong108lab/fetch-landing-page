import React from 'react'
import './AppRow8.css'
import arrowRight from '../../../public/assets/arrowRight.png'
import arrowDown from '../../../public/assets/arrowDown.png'
import QuestionComp from './QuestionComp.js'

const questions = [
    {
        question: 'Under FETCH managed service, who is the legal employer of the employee?',
        answer: 'Unless your company has a legal entity in Vietnam, Fetch will be the legal employer. We are able to transfer out the employee to your company, if you require us to.'
    }, {
        question: 'Is there a probation period for my employee?',
        answer: 'Yes, every employee has a 2 months probation. And you will be allowed to terminate with immediate effect if the employee is not performing up to expectation during his probation.'
    }, {
        question: 'Can I bring my staff overseas for training or short term work?',
        answer: 'Yes, we can help with the necessary arrangements.'
    }, {
        question: 'Do I need to pay recruitment fees again if my staff resigns or if I terminate the staff?',
        answer: 'No, not within the first year. We provide free replacements for the same position.'
    }, {
        question: 'What is the minimum full-time contract period?',
        answer: '12 Months in accordance to labour laws in Vietnam. This doesn’t mean you cannot let the employee go if he/she isn’t performing. 3 official warnings, through Fetch, and you will be allowed to terminate the contract in 7 days without incurring any severance for your employee.'
    }, {
        question: 'What equipment are provided under the managed service?',
        answer: 'Fetch’s offices are fully equipped with all common office supplies. A personal working laptop will be assigned to the employee as his personal working equipment. We are able to help you manage your devices if you require to assign more specialized equipment or prefer to assign the employee your own laptop.'
    }, {
        question: 'Can I relocate my staff to another country permanently?',
        answer: 'Yes, subjected to mutual agreement between you and the staff and your own company’s successful application for work permit and visa needs. In such cases, we will transfer out the legal employment to you entirely.'
    }, {
        question: 'Can I or any of my colleague visit the office to work with our employee?',
        answer: 'Yes, definitely. A desk will be assigned to anyone from your company during the duration of their stay with us. Our office functions like your own office.'
    }
]

const AppRow8 = () => {

    console.log('render in parent')

    return (
        <div className="row8__container">
            <div className="row8__container-content">
                <div className="row8__container-row">
                    <div className="row8__container-col12">
                        <h2>FAQ</h2>
                        <h3>Feel free to let us know what other questions you may have!</h3>
                    </div>
                </div>
                <div className="row8__container-row">
                    <div className="row8__container-col6">
                            {questions.map((obj, index) => index < questions.length/2 ? (
                                <QuestionComp 
                                    content = {obj}
                                    arrowDown = {arrowDown}
                                    arrowRight = {arrowRight}
                                />
                            ) : (<></>))}

                    </div>
                    <div className="row8__container-col6">
                            {questions.map((obj, index) => index >= questions.length/2 ? (
                                <QuestionComp 
                                    content = {obj}
                                    arrowDown = {arrowDown}
                                    arrowRight = {arrowRight}
                                />
                            ) : (<></>))}
                        
                    </div>
                </div>
                <div className="row8__container-row">
                    <div className="row8__container-col12">
                        <div className="btn-build">BUILD ME A TEAM NOW</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AppRow8
