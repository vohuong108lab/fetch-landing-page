import React from 'react'
import './AppRow2.css'
import image7 from '../../../assets/7.jpg'
import image3 from '../../../assets/3.jpg'

const AppRow2 = () => {
    return (
        <div className="row2__container">
            <div className="row2__container-content">
                <div className="row2__container-row">
                    <div className="row2__container-col4">
                        <h2>We Understand Hiring Can Be A Real Pain</h2>
                        <div className="col4-left-div"></div>
                        <p>We Understand Hiring Can Be A Real Pain</p>
                        <p>We are here to change all of that, so that you have a working tech team up and running in weeks!</p>
                    </div>
                    <div className="row2__container-col4">
                        <img className="" src={image7} alt="" />
                        <h3>We Keep It Simple, Transparent & Fuss free</h3>
                        <p>Say goodbyes to uncertainties due to improper HR engagements.</p>
                        <p>Like you, we strongly believe that you must know what you are getting into. We will take you through it in person.</p>
                    </div>
                    <div className="row2__container-col4">
                        <h3>We Get The Fairest Rates</h3>
                        <p>Leave the negotiations to us. Our combined experiences hiring thousands of developers ensure that you are always getting great developers at market rates.</p>
                        <img className="" src={image3} alt="" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AppRow2
