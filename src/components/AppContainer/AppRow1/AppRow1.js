import React from 'react'
import './AppRow1.css'

const AppRow1 = () => {
    return (
        <div className="row1__container">
            <div className="row1__container-content">
                <div className="row1__container-row">
                    <div className="row1__container-left">
                        <h1>Hire top engineering talents from Vietnam</h1>
                        <h2>Fetch helps Singapore companies hire the best software developers, testers, designers in Vietnam. Fill up the form to start hiring</h2>
                    </div>
                    <div className="row1__container-right">
                        <div className="header__form">Trusted Freelance Talent, Ready To Join Your Team</div>
                        <div className="wrapper__form">
                            <form className="row1__form">
                                <div className="row1__form-content-col6">
                                    <div className="row1__form-content-col6-col">
                                        <label>Name<span>*</span></label>
                                        <input className="row1_form-input" type="text" required></input>
                                    </div>

                                    <div className="row1__form-content-col6-col">
                                        <label>Email address<span>*</span></label>
                                        <input className="row1_form-input" type="text" required></input>
                                    </div>

                                    <div className="row1__form-content-col6-col">
                                        <label>Phone<span>*</span></label>
                                        <input className="row1_form-input" type="text" required></input>
                                    </div>

                                    <div className="row1__form-content-col6-col">
                                        <label>Company name<span>*</span></label>
                                        <input className="row1_form-input" type="text" required></input>
                                    </div>

                                </div>

                                <div className="row1__form-content-col12">
                                    <div className="row1__form-content-col12-col">
                                        <input type="checkbox"></input>
                                        <label>I consent to be contacted by FETCH for any follow up action regarding my request to build a technical team enquiry</label>
                                    </div>
                                </div>

                                <div className="row1__form-content-col6">
                                    <div className="row1__form-content-col6-col peusedo-col"></div>
                                    <div className="row1__form-content-col6-col md-col6">
                                        <button type="submit" className="btn">Let's start hiring</button>
                                    </div>
                                </div>

                                <div className="row1__form-content-col12">
                                    <div className="row1__form-content-col12-col disclaimer">
                                        You'll receive a non-obligatory call from us to help you with your hiring needs. No risks involved. Ask us anytime. Hang up anytime.
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default AppRow1;
