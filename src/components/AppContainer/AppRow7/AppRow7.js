import React, { useState, useLayoutEffect } from 'react'
import './AppRow7.css'

const AppRow7 = () => {
    const [bottomHeight, setBottomHeight] = useState(50);

    useLayoutEffect(() => {
        const updateSize = () => {
            let padding = 15;
            let lineHeight = 15;
            const elementList = document.getElementsByClassName('line-content');
            let heightLastChild = elementList[elementList.length - 1].clientHeight;

            // console.log(heightLastChild)
            setBottomHeight(heightLastChild - padding - lineHeight);
        }

        window.addEventListener('resize', updateSize);
        updateSize();
        return () => window.removeEventListener('resize', updateSize);
    }, []);

    return (
        <div className="row7__container">
            <div className="row7__container-content">
                <div className="row7__container-row">
                    <div className="row7__container-col12">
                        <h2>A small step can make a big difference</h2>
                    </div>
                </div>
                <div className="row7__container-row">
                    <div className="row7__container-col6">
                        <div className="wrapper-col6">
                            <div 
                                className="vertical-line"
                                style={{
                                    bottom: `${bottomHeight}px`
                                }}
                            ></div>
                            <div className="wrap-line">
                                <p className="line-content">
                                    Tired of unethical practices and overpricing within the software development industry, we came together to form Fetch Technology to help Singapore companies get started on their product fast with scalable, sustainable and talented tech teams.
                                </p>
                                <p className="line-content">
                                    Since each of us came from the tech or recruitment industry, we have seen a fair bit of victims and most of it can actually be avoided
                                </p>
                                <p className="line-content">
                                    We value and cherish long term relationships instead of one off transactions
                                </p>
                                <p className="line-content">
                                    We are committed in making sure no company misses out on this extraordinary software engineering talent pool that Vietnam has to offer.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AppRow7;