import React from 'react'
import './Footer.css'
import LogoGroup from '../../public/assets/Group2167.svg'

const Footer = () => {
    return (
        <footer id="footer">
            <div className="footer__container">
                <div className="footer__container-row">
                    <div className="footer__container-col3">
                        <img className="logo-group" src={LogoGroup} alt="" />
                    </div>
                    <div className="footer__container-col3">
                        <div className="col3-title">CONTACTS</div>
                        <div className="col3-wrap">
                            <div className="col3-content">
                                <span className="head-content">E.</span>
                                <span>sales@fetch.tech</span>

                            </div>
                            <div className="col3-content">
                                <span className="head-content">T.</span>
                                <span>+65 6383 3833</span>

                            </div>
                            <div className="col3-content">
                                <span className="head-content">A.</span>
                                <span>76 PLAYFAIR ROAD SINGAPORE 367996</span>

                            </div>
                        </div>
                    </div>
                    <div className="footer__container-col3">
                        <div className="col3-title">FOLLOW</div>
                        <div className="col3-wrap">
                            <div className="col3-social">
                                <span>
                                    <i class="fab fa-linkedin-in"></i>
                                </span>
                                <span>
                                    <i class="fab fa-twitter"></i>
                                </span>
                                <span>
                                    <i class="fab fa-facebook-f"></i>
                                </span>
                                <span>
                                    <i class="fab fa-instagram"></i>
                                </span>

                            </div>
                            
                        </div>
                    </div>
                    <div className="footer__container-col3">
                        <div className="col3-title">COPYRIGHT</div>
                        <div className="col3-wrap">
                            <div className="col3-content col3-copyright">
                                © 2020 Fetch Technology Pte. Ltd. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer__container-row-footer">
                    © 2020 Fetch Technology Pte. Ltd. All Rights Reserved.
                </div>
            </div>
        </footer>
    )
}

export default Footer
