import './App.css';
import Header from './components/Header/Header.js';
import AppContainer from './components/AppContainer/AppContainer.js';
import Footer from './components/Footer/Footer.js';

function App() {
  return (
    <div className="App">
      <Header />
      <AppContainer />
      <Footer />
    </div>
  );
}

export default App;
